# Alchemy

A simple yet powerful way to create C++ GUIs using HTML5, CSS3, and more!

# What does it look like?

In your C++:

    :::cpp
        #include "alchemy/alchemy.h"
        #include <iostream>
        using namespace std;
        using namespace Alchemy;

        string getUserName() {
            string name;
            cout << "Enter name: ";
            getline(cin, s);
            return s;
        }

        int main() {
            Settings s;
            s.Directory("ui");

            auto app = Application::Create(s);
            app->Page("index.html")->Bind("getUserName", getUserName);
            app->Show("index.html");
        }

And the `index.html` page:

    :::html
        <html>
        <head>
            <title>Demo</title>
            <script>
                window.onload = function() {
                    document.getElementById('name').innerText = app.getUserName();  
                };
        </head>
        <body>
            Hello, <span id="name"></span>
        </body>
        </html>

# What else does it do?

* JS/C++ Type Conversion to and from strings, numbers, booleans, functions, objects, and arrays
* Call C++ functions from JavaScript
* Call JavaScript functions from C++
* Manage windows from JavaScript
* Runs on Mac, Windows, and Linux

# Where can I learn more?

Visit our [official website, http://libalchemy.org](http://libalchemy.org) for downloads, documentation, tutorials, and other resources