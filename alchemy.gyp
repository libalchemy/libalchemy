{
  'includes': [
    'gyp/alchemy_paths.gypi',
  ],
  'variables': {
    'cef_path': 'cef',
    'gtest_path': 'gtest',
  },
  'target_defaults': {
    'conditions': [
      ['OS=="linux"', {'defines': ['__LINUX__']}],
      ['OS=="mac"', {
        'defines': ['__MAC__'],
        'xcode_settings': {
          'CLANG_CXX_LANGUAGE_STANDARD': 'c++0x',
          'CLANG_CXX_LIBRARY': 'libc++'
        }
      }],
      ['OS=="win"', {
        'defines': [
          '__WIN__',
          'UNICODE',
          'NOMINMAX',
        ]
      }],
    ],
    'cflags': [
      '-Wall',
      '-std=c++0x'
    ],
    'msbuild_toolset': 'v120_CTP_Nov2012',
    'configurations': {
      'Debug': {
        'defines': [
          'DEBUG',
        ],
        'msvs_settings': {
          'VCCLCompilerTool': {
            'Optimization': '0',
          },
          'VCLinkerTool': {
            'GenerateDebugInformation': 'true',
          },
        },
        'xcode_settings': {
          'GCC_GENERATE_DEBUGGING_SYMBOLS ': 'YES',
          'GCC_DEBUGGING_SYMBOLS': 'full',
          'DEBUG_INFORMATION_FORMAT': 'dwarf-with-dsym',
          'GCC_OPTIMIZATION_LEVEL': '0',
        },
      },
      'Release': {
        'defines': [
          'RELEASE',
        ],
      },
    },
  },
  'targets': [
    {
      # the core library of the alchemy project
      # produces the libalchemy library
      'target_name': 'alchemy',
      'type': 'static_library',
      'include_dirs': [
        '<(alchemy_path)',
        '<(cef_path)'
      ],
      'sources': [
        '<@(alchemy_sources_common)',
        '<@(alchemy_includes_common)',
        '<@(alchemy_includes_impl)'
      ],
      'conditions': [
        ['OS=="linux"', {
          'sources': [
            '<@(alchemy_sources_linux)',
          ],
          'cflags': [
            '<!@(pkg-config --cflags gtk+-2.0 gthread-2.0)',
          ],
          'ldflags': [
            '<!@(pkg-config --libs-only-L --libs-only-other gtk+-2.0 gthread-2.0)',
          ],
          'libraries': [
            '<!@(pkg-config --libs-only-l gtk+-2.0 gthread-2.0)',
            '<(cef_path)/libcef_dll_wrapper.a',
            '<(cef_path)/libcef.so',
          ],
        }],
        ['OS=="mac"', {
          'sources': [
            '<@(alchemy_sources_mac)'
          ],
          'libraries': [
            '$(SDKROOT)/System/Library/Frameworks/AppKit.framework',
            '<(cef_path)/libcef_dll_wrapper.a',
            '<(cef_path)/libcef.dylib',
          ]
        }],
        ['OS=="win"', {
          'sources': [
            '<@(alchemy_sources_win)'
          ],
        }],
      ],
    },
    {
      'target_name': 'distrib',
      'type': 'none',
      'dependencies': [
        'alchemy'
      ],
      'copies': [
        {
          'destination': '<(PRODUCT_DIR)/distrib/include/alchemy',
          'files': ['<@(alchemy_includes_common)']
        },
        {
          'destination': '<(PRODUCT_DIR)/distrib/lib/',
          'files': ['<(PRODUCT_DIR)/libalchemy.a']
        }
      ]
    },
    {
      'target_name': 'tests',
      'type': 'executable',
      'mac_bundle': 1,
      'dependencies': [
        'alchemy'
      ],
      'include_dirs': [
        '<(cef_path)',
        '<(alchemy_path)',
        '<(gtest_path)',
      ],
      'sources': [
        '<@(alchemy_tests)',
      ],
      'conditions': [
        ['OS=="mac"', {
          'defines': [
            'GTEST_USE_OWN_TR1_TUPLE'
          ],
          'libraries': [
            '$(SDKROOT)/System/Library/Frameworks/AppKit.framework',
            '<(gtest_path)/libgtest_main.a',
            # '<(gtest_path)/libgtest.a',
            '<(cef_path)/libcef.dylib'
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/tests.app/Contents/MacOS',
              'files': ['<(cef_path)/libcef.dylib']
            },
            {
              'destination': '<(PRODUCT_DIR)/tests.app/Contents',
              'files': [
                '<(cef_path)/Resources',
                'src/test/ui',
              ]
            }
          ],
        }],
        ['OS=="win"', {
          'defines': [
            '_VARIADIC_MAX=10',
          ],
          'link_settings': {
            'libraries': [
              '<(gtest_path)/gtest_main.lib',
              '<(gtest_path)/gtest.lib',
            ]
          },
          'msvs_settings': {
            'VCLinkerTool': {
              'SubSystem': '1',  # SubSystem = Console (/SUBSYSTEM:CONSOLE)
            }
          },
          'libraries': [
            '<(cef_path)/libcef_dll_wrapper.lib',
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/',
              'files': [
                '<(cef_path)/runtime/d3dcompiler_43.dll',
                '<(cef_path)/runtime/d3dx9_43.dll',
                '<(cef_path)/runtime/devtools_resources.pak',
                '<(cef_path)/runtime/icudt.dll',
                '<(cef_path)/runtime/libcef.dll',
                '<(cef_path)/runtime/libEGL.dll',
                '<(cef_path)/runtime/libGLESv2.dll',
                '<(cef_path)/libcef.lib',
              ]
            },
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/runtime/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)/ui',
              'files': [
                '<(test_path)/ui/index.html'
              ]
            },
          ],
        }],
        ['OS=="linux"', {
          'libraries': [
            '<!@(pkg-config --libs-only-l gtk+-2.0 gthread-2.0)',
            '<(cef_path)/libcef.so',
            '<(cef_path)/libcef_dll_wrapper.a',
            '<(gtest_path)/libgtest_main.a',
            '<(gtest_path)/libgtest.a'
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/chrome.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/libcef.so']
            },
            {
              'destination': '<(PRODUCT_DIR)/ui',
              'files': ['<(test_path)/ui/index.html']
            }
          ],
        }],
      ],
    }
  ],
}
