#ifndef ALCHEMY_UI_TYPES_H
#define ALCHEMY_UI_TYPES_H
#pragma once
/**
*
* Defines platform-dependent types needed by the window component
*
* Note that this does not and should not include the headers for the types,
* only forward-declare the platform-dependent type and typedef for the independent
* type.
*/

#ifdef __LINUX__
#include <gtk/gtk.h>
#define WindowHandle GtkWidget*
#endif

// When included by a .cpp fle in xcode, __MAC__ will be defined, but __OBJC__
// will not. When included by a .mm file in xcode, both will be defined.
// If we're compiling a C++ file, consider NSView to be a simple struct,
// otherwise use the standard Objective-C type.
#ifdef __MAC__
#ifdef __OBJC__
@class NSView;
#else
struct NSView;
#endif

// For whatever reason, Objective-C requires the above forward declaration to
// be in the global namespace, so *now* we'll namespace the typedef.
namespace Alchemy {
namespace UI {
typedef ::NSView* WindowHandle;
}
}
#endif // __MAC__

#ifdef __WIN__
#include <windows.h>
typedef HWND WindowHandle;
#endif //__WIN__

#endif // Guard