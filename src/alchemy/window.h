#ifndef ALCHEMY_UI_WINDOW_H
#define ALCHEMY_UI_WINDOW_H
#pragma once

#include "alchemy/ui_types.h"
#include "alchemy/page.h"
#include "alchemy/client_impl.h"
#include <string>

namespace Alchemy{
namespace UI{

class Window{
public:
  /** Constructs a window */
  Window(const Settings& settings);

  /**
  * Loads given page into the shown window.
  *
  * @param[in] The current client
  * @param[in] The page to load
  */
  void Load(ClientImpl* client, Page* page);

  /**
   * Returns width of the screen.
   *
   * This is considered to be the width of the area where the window can
   * be positioned, rather than the width of the entire visible screen.
   */
  static int ScreenWidth();

  /**
   * Returns the height of the screen.
   *
   * This is considered to be the hight of the area where the window can
   * be positioned, rather than the height of the entire visible screen.
   */
  static int ScreenHeight();

  /** Shows the window and blocks until it closes. */
  void Show();

  /** Closes the window */
  void Close();

  /** Minimizes the window */
  void Minimize();

  /** Maximizes the window */
  void Maximize();

  /** Restores the window to its size before being minimized or maximized */
  void Restore();

  /**
   * Set the title of the window
   *
   *@param[in] The title of the window
   */
  void SetTitle(const std::string& title);

  /**
   * Set the size of the window
   *
   * @param[in] The width of the window
   * @param[in] The height of the window
   */
  void SetSize(int width, int height);

  /**
   * Set the position of the window
   *
   * @param[in] The left edge of the window
   * @param[in] The top edge of the window
   */
  void SetPosition(int x, int y);

  /** Get the x-coordinate of the left edge of the window */
  int GetX();

  /** Get the y-coordinate of the top edge of the window */
  int GetY();

  /** Get the current width of the window */
  int GetWidth();

  /** Get the current height of the window */
  int GetHeight();

private:
  /** Whether global window initialization was already performed */
  static bool _didGlobalInit;
  /** Initializes the native UI toolkit before the first window is created */
  static void GlobalInit();
  /** The hieght of the title bar on the window */
  static int TitleBarHeight();

  /** Pointer to the native window object */
  WindowHandle _handle;

  /** Pointer to the settings object */
  Settings _settings;

  /** Initializes the native window object */
  void Init(int width, int height);
};

} // namespace UI
} // namespace Alchemy

#endif