#include <string>
#include "alchemy/window.h"

#include "alchemy/logger.h"

namespace Alchemy {
namespace UI {

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
  //adapted from cefclient_win.cpp (in chromium embedded cefclient)
  auto childHwnd = GetWindow(hWnd, GW_CHILD);

  switch (message) {
    case WM_SETFOCUS:
      if (childHwnd) {
        // Pass focus to the browser window
        PostMessage(childHwnd, WM_SETFOCUS, wParam, NULL);
      }
      return 0;

    case WM_PAINT:
      PAINTSTRUCT ps;
      HDC hdc;

      hdc = BeginPaint(hWnd, &ps);
      EndPaint(hWnd, &ps);
      return 0;

    case WM_SIZE:
      if (childHwnd) {
        // Resize the browser window to match the new frame window size
        RECT rect;
        GetClientRect(hWnd, &rect);

        HDWP hdwp = BeginDeferWindowPos(1);
        hdwp = DeferWindowPos(hdwp, childHwnd, NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOZORDER);
        EndDeferWindowPos(hdwp);
        break;
      }

    case WM_ERASEBKGND:
      // Dont erase the background if the browser window has been loaded (this avoids flashing)
      if (childHwnd) return 0;
      break;

    case WM_DESTROY:
      // The frame window has exited
      PostQuitMessage(0);
      return 0;
  }

  return DefWindowProc(hWnd, message, wParam, lParam);
}

void Window::GlobalInit() {
  WNDCLASSEX wcex;

  wcex.cbSize = sizeof(WNDCLASSEX);

  wcex.style         = CS_HREDRAW | CS_VREDRAW;
  wcex.lpfnWndProc   = WndProc;
  wcex.cbClsExtra    = 0;
  wcex.cbWndExtra    = 0;
  wcex.hInstance     = GetModuleHandle(NULL);
  wcex.hIcon         = NULL;
  wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
  wcex.lpszMenuName  = 0;
  wcex.lpszClassName = L"Alchemy";
  wcex.hIconSm       = NULL;

  RegisterClassEx(&wcex);
}

void Window::Init(int width, int height) {
  _handle = CreateWindow(L"Alchemy", L"Alchemy",
                         WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, CW_USEDEFAULT, 0,
                         width, height, NULL, NULL, GetModuleHandle(NULL), NULL);
}

void Window::Load(ClientImpl* client, Page* page) {
  CefBrowserSettings settings;
  CefWindowInfo info;
  RECT rect;

  GetClientRect(_handle, &rect);
  info.SetAsChild(_handle, rect);

  CefBrowser::CreateBrowser(info, client, page->GetURL(), settings);
}

void Window::Show() {
  ShowWindow(_handle, SW_SHOWDEFAULT);
  UpdateWindow(_handle);
}

void Window::Close() {
  PostMessage(_handle, WM_CLOSE, NULL, NULL);
}

int Window::ScreenWidth() {
  return GetSystemMetrics(SM_CXSCREEN);
}

int Window::ScreenHeight() {
  return GetSystemMetrics(SM_CYSCREEN);
}

int Window::TitleBarHeight() {
  return GetSystemMetrics(SM_CYCAPTION);
}

void Window::Minimize() {
  ShowWindow(_handle, SW_MINIMIZE);
}

void Window::Maximize() {
  ShowWindow(_handle, SW_MAXIMIZE);
}

void Window::Restore() {
  ShowWindow(_handle, SW_RESTORE);
}

void Window::SetTitle(const std::string& title) {
  std::wstring t(title.begin(), title.end());
  SetWindowText(_handle, t.c_str());
}

void Window::SetPosition(int x, int y) {
  SetWindowPos(_handle, NULL, x, y, GetWidth(), GetHeight(), NULL);
}

void Window::SetSize(int width, int height) {
  SetWindowPos(_handle, NULL, GetX(), GetY(), width, height, NULL);
}

int Window::GetX() {
  RECT rect;
  GetWindowRect(_handle, &rect);

  return rect.left;
}

int Window::GetY() {
  RECT rect;
  GetWindowRect(_handle, &rect);

  return rect.top;
}

int Window::GetWidth() {
  RECT rect;
  GetWindowRect(_handle, &rect);

  return abs(rect.right - rect.left);
}

int Window::GetHeight() {
  RECT rect;
  GetWindowRect(_handle, &rect);

  return abs(rect.top - rect.bottom);
}

}}