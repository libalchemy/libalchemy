#include <memory>
#include "alchemy/application.h"
#include "alchemy/application_impl.h"

namespace Alchemy {

std::unique_ptr<Application> Application::Create(const Settings& settings) {
  return make_unique<ApplicationImpl>(settings);
}

} // namespace Alchemy