#ifndef ALCHEMY_JS_JS_H
#define ALCHEMY_JS_JS_H
#pragma once

#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <map>

#include "alchemy/utils.h"

#ifdef __WIN__
  //comes with the default headers in Visual Studio
  #undef GetObject
#endif

namespace Alchemy {
namespace JS {

class Object;
class Array;
class Function;
class FunctionHandler;
template <typename, typename...> class FunctionPointerHandler;

class NativeType {
public:
  // Static Members
  struct NullType { };
  struct UndefinedType { };

  static NativeType Null();
  static NativeType Undefined();

  // Default/Copy/Move constructors
  NativeType();
  NativeType(const NativeType& other);
  NativeType(NativeType&& other);

  // Conversion constructors
  NativeType(int value);
  NativeType(unsigned int value);
  NativeType(double value);
  NativeType(bool value);
  NativeType(const char* value);
  NativeType(const std::string& value);
  NativeType(const JS::Function& handler);
  NativeType(const Object& value);
  NativeType(const Array& value);

  ~NativeType();

  // Copy/Move Assignment
  NativeType& operator=(const NativeType& other);
  NativeType& operator=(NativeType&& other);

  std::string GetTypeString() const;
  std::string GetDebugString() const;

  // Type Checking
  bool IsNull() const;
  bool IsUndefined() const;
  bool IsInt() const;
  bool IsUInt() const;
  bool IsDouble() const;
  bool IsBool() const;
  bool IsString() const;
  bool IsFunction() const;
  bool IsObject() const;
  bool IsArray() const;

  bool IsTruthy() const;

  // Value Retrieval (CHECK TYPES FIRST!)
  int GetInt() const;
  unsigned int GetUInt() const;
  double GetDouble() const;
  bool GetBool() const;
  std::string GetString() const;
  JS::Function GetFunction() const;
  Object GetObject() const;
  Array GetArray() const;

  // Casting
  operator int() const;
  operator unsigned int() const;
  operator double() const;
  operator bool() const;
  operator std::string() const;
  operator JS::Function() const;
  operator Object() const;
  operator Array() const;

private:
  struct Impl;
  std::unique_ptr<Impl> _impl;

  NativeType(const NullType& value);
  NativeType(const UndefinedType& value);
};

typedef std::vector<NativeType> NativeTypeList;

NativeType&& Or(NativeType&& a, NativeType&& b);

class FunctionHandler {
public:
  FunctionHandler(const std::string& name) : _name(name) { }
  virtual ~FunctionHandler() { }

  std::string Name() const { return _name; }

  template <typename... Args>
  NativeType Invoke(const Args&... args) {
    return InvokeWithArgs(CollectArgs<NativeType, Args...>(args...));
  }

  virtual NativeType InvokeWithArgs(const NativeTypeList& args) const = 0;

private:
  std::string _name;
};

template <typename R, typename... Args>
class FunctionPointerHandler : public FunctionHandler {
public:
  //function and static member function
  FunctionPointerHandler(const std::string& name, R(*fp)(Args...))
  : FunctionHandler(name), _function(fp) { }

  //member function
  template <typename T>
  FunctionPointerHandler(const std::string& name, R(T::*fp)(Args...), T obj)
  : FunctionHandler(name), _function(MemberPointer<T>(obj, fp)) { }

  //std::function
  FunctionPointerHandler(const std::string& name, const std::function<R(Args...)>& fn)
  : FunctionHandler(name), _function(fn) { }

  virtual NativeType InvokeWithArgs(const NativeTypeList& args) const override {
    return _Invoke<0>(args);
  }

  // there are still un-unrolled arguments.
  template <size_t index, typename... Ts>
  typename std::enable_if<index < sizeof...(Args), NativeType>::type
  _Invoke(const NativeTypeList& arguments, Ts... unrolled) const {
    return _Invoke<index+1>(arguments, unrolled..., arguments[index]);
  }

  template <size_t index, typename... Ts>
  typename std::enable_if<index == sizeof...(Args), NativeType>::type
  _Invoke(const NativeTypeList& arguments, Ts... unrolled) const {
    return Invoke<std::is_void<R>::value>(unrolled...);
  }

  // all arguments have been unrolled at this point
  template <bool isVoid>
  auto Invoke(Args... args) const
  -> typename std::enable_if<!isVoid, NativeType>::type {
    return _function(args...);
  }

  template <bool isVoid>
  auto Invoke(Args... args) const
  -> typename std::enable_if<isVoid, NativeType>::type {
    _function(args...);
    return NativeType::Undefined();
  }

private:
  std::function<R(Args...)> _function;

  template <typename T>
  struct MemberPointer {
    MemberPointer(const T& obj, R(T::*fp)(Args...)) : _obj(obj), _fp(fp) { }
    R operator()(Args&&... args) { return (_obj.*_fp)(std::forward<Args>(args)...); }
    T _obj; R(T::*_fp)(Args...);
  };
};

class Function {
public:
  template <typename R, typename... Args>
  Function(const std::string& name, R(*fp)(Args...))
  : _handler(std::make_shared<FunctionPointerHandler<R, Args...>>(name, fp)) { }

  template <typename R, typename... Args>
  Function(const std::string& name, const std::function<R(Args...)>& fn)
  : _handler(std::make_shared<FunctionPointerHandler<R, Args...>>(name, fn)) { }

  template <typename T, typename R, typename... Args>
  Function(const std::string& name, R(T::*fp)(Args...), T obj)
  : _handler(std::make_shared<FunctionPointerHandler<R, Args...>>(name, fp, obj)) { }

  Function();
  Function(const Function& other);
  Function(Function&& other);
  Function(FunctionHandler* other);
  ~Function();
  Function& operator=(const Function& other);
  Function& operator=(Function&& other);

  std::string Name() const {
    return _handler->Name();
  }

  template <typename... Args>
  NativeType operator()(Args... args) const {
    return _handler->Invoke(args...);
  }

  FunctionHandler* GetHandler();

private:
  std::shared_ptr<FunctionHandler> _handler;
};

// @TODO: Support object prototypes
class Object {
public:
  typedef std::map<std::string, NativeType> MapType;

  Object();
  Object(const Object& other);
  Object(Object&& other);
  Object(const MapType& map);

  ~Object();

  Object& operator=(const Object& other);
  Object& operator=(Object&& other);

  void Set(const std::string& key, const NativeType& value);
  NativeType Get(const std::string& key) const;

  // Expose access to the underlying std::map
  // Yes, this violates encapsulation, but also makes it very compatible
  // with the STL
  MapType& GetMap();
  const MapType& GetMap() const;

  /**
   * Merge two Objects together, returning a new Object.
   * Any properties in the second overwrite those in the first.
   */
  static Object Merge(const Object& a, const Object& b);

private:
  MapType _map;
};

class Array {
public:
  typedef std::vector<NativeType> VectorType;

  Array();
  Array(const Array& other);
  Array(Array&& other);
  Array(const VectorType& map);

  ~Array();

  Array& operator=(const Array& other);
  Array& operator=(Array&& other);

  void Set(int index, const NativeType& value);
  NativeType Get(int index) const;

  // Expose access to the underlying std::map
  // Yes, this violates encapsulation, but also makes it very compatible
  // with the STL
  VectorType& GetVector();
  const VectorType& GetVector() const;

  // Returns the length of the array
  int Length() const;

private:
  VectorType _vector;
};

} // namespace JS
} // namespace Alchemy

#endif // ALCHEMY_JS_JS_H