#include <stdexcept>
#include <iostream>
#include "alchemy/utils.h"

namespace Alchemy {

NORETURN void ThrowException(std::string msg) {
  std::cerr << msg;
  throw std::runtime_error(msg);
}

}
