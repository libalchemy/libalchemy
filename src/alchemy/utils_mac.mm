#import <Foundation/Foundation.h>
#include <mach-o/dyld.h>
#include <string>

namespace Alchemy {

// stolen from CEF's cefclient demo
bool AmIBundled() {
  // Implementation adapted from Chromium's base/mac/foundation_util.mm
  ProcessSerialNumber psn = {0, kCurrentProcess};

  FSRef fsref;
  OSStatus pbErr;
  if ((pbErr = GetProcessBundleLocation(&psn, &fsref)) != noErr) {
    return false;
  }

  FSCatalogInfo info;
  OSErr fsErr;
  if ((fsErr = FSGetCatalogInfo(&fsref, kFSCatInfoNodeFlags, &info,
                                NULL, NULL, NULL)) != noErr) {
    return false;
  }

  return (info.nodeFlags & kFSNodeIsDirectoryMask);
}

std::string GetExecutableFolder() {
  std::string dir;
  // Implementation adapted from Chromium's base/base_path_mac.mm
  if (AmIBundled()) {
    // Retrieve the executable directory.
    uint32_t pathSize = 0;
    _NSGetExecutablePath(NULL, &pathSize);
    if (pathSize > 0) {
      dir.resize(pathSize);
      _NSGetExecutablePath(const_cast<char*>(dir.c_str()), &pathSize);
    }

    // Trim executable name up to the last separator
    std::string::size_type last_separator = dir.rfind("/");
    dir = dir.substr(0, last_separator);
    last_separator = dir.rfind("/");
    dir = dir.substr(0, last_separator);
    return dir;
  } else {
    // TODO: Provide unbundled path
    return "";
  }
}

} // namespace Alchemy