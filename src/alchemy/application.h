#ifndef ALCHEMY_APPLICATION_H
#define ALCHEMY_APPLICATION_H
#pragma once

#include <string>
#include <memory>
#include "alchemy/settings.h"

namespace Alchemy {

class Page;

class Application {
public:
  virtual ~Application() { }
  static std::unique_ptr<Application> Create();
  static std::unique_ptr<Application> Create(const Settings& settings);

  virtual void Show(const std::string& name) = 0;
  virtual void Show(Page* page) = 0;

  virtual void Close() = 0;

  virtual Alchemy::Page* Page(const std::string& name) = 0;

  virtual Alchemy::Page* GetPageByURL(const std::string& url) = 0;
  virtual bool PageExists(std::string pageName) = 0;

  virtual bool IsDevToolsShown() = 0;
};

} // namespace Alchemy

#endif