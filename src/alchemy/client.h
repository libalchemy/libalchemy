#ifndef ALCHEMY_CLIENT_H
#define ALCHEMY_CLIENT_H
#pragma once

#include <string>

#include "alchemy/js.h"

namespace Alchemy {

class Page;

class Client {
public:
  virtual ~Client() { }
  virtual JS::NativeType CallFunction(Page* page, const std::string& name, const JS::NativeTypeList& args) = 0;
};

} // namespace Alchemy

#endif // ALCHEMY_CLIENT_H