#ifndef ALCHEMY_APPLICATION_IMPL_H
#define ALCHEMY_APPLICATION_IMPL_H
#pragma once

#include "alchemy/application.h"
#include "alchemy/client_impl.h"
#include "alchemy/window.h"

namespace Alchemy {

class Page;

class ApplicationImpl : public Alchemy::Application {
public:
  /**
  * Constructs a new ApplicationImpl object.
  *
  *@param[in] Settings object
  */
  ApplicationImpl(const Settings& settings);
  /**
  * ApplicationImpl destructor
  */
  ~ApplicationImpl();

  /**
  * Shows the page with the given name in the window.
  *
  *@param[in] Name of page to show
  */
  virtual void Show(const std::string& name);
  /**
  * Shows the given page in the window.
  *
  *@param[in] Page to show
  */
  virtual void Show(Alchemy::Page* page);

  /**
  * Closes the active window by quitting the message loop.
  */
  virtual void Close();
 
  /**
  * Returns the page object that matches the given name.
  *
  *@param[in] Name of the page you want returned
  */
  virtual Alchemy::Page* Page(const std::string& name);

  /**
  * Returns the page object that corresponds to the given URL.
  *
  *@param[in] URL of the page you want returned
  */
  virtual Alchemy::Page* GetPageByURL(const std::string& url);
 
  /**
  * Returns whether the page that matches the given name exists.
  *
  *@param[in] Name of the page you want to check exists
  */
  virtual bool PageExists(std::string pageName);

  /**
  * Returns the URL prefix (the path to the current execution directory and settings directory)
  */
  std::string GetURLPrefix();

  /**
  * Returns the window object.
  */
  UI::Window GetWindow();

  /**
  * Returns the settings object.
  */
  Alchemy::Settings GetSettings();

  /**
  * Returns a flag value for testing dev tools
  */
  virtual bool IsDevToolsShown();

private:
  /** A type for handling mapping pages to string names. */
  typedef std::map<std::string, std::unique_ptr<Alchemy::Page>> PageMap;

  /** Pointer to client object */
  std::unique_ptr<ClientImpl> _client;
  /** Pointer to window object */
  UI::Window _window;
  /** Pointer to settings object */
  Alchemy::Settings _settings;
  /** Map of pages to string page names */
  PageMap _pages;
  /** Mutex for thread-safe member access */ 
  std::mutex _lock;
};

} // namespace Alchemy

#endif