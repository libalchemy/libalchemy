#ifndef ALCHEMY_ALCHEMY_H
#define ALCHEMY_ALCHEMY_H
#pragma once

#include "alchemy/application.h"
#include "alchemy/page_handler.h"
#include "alchemy/page.h"

#endif //ALCHEMY_ALCHEMY_H