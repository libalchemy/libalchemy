#include <memory>
#include "alchemy/window.h"
#include "alchemy/page.h"
#include "alchemy/client_impl.h"
#include "include/cef_browser.h"

namespace Alchemy{
namespace UI{

bool Window::_didGlobalInit = false;

Window::Window(const Settings& settings)
: _settings(settings) {
  if(!_didGlobalInit){
    GlobalInit();
    _didGlobalInit = true;
  }

  Size s = settings.DefaultWindowSize();
  Init(s.width, s.height);
}

} // namespace UI
} // namespace Alchemy