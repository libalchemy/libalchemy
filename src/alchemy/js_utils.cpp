#include "alchemy/js_utils.h"
#include "alchemy/utils.h"

#include "include/cef_v8.h"
#include "include/cef_task.h"
#include "include/cef_runnable.h"

#include <iostream>
#include <future>
#include <algorithm>

namespace Alchemy {
namespace JS {

struct NativeFunctionHandler : public CefV8Handler {
  NativeFunctionHandler(FunctionHandler* f) : _function(f) { };

  //calls to Execute will always occur within a v8 context.
  bool Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval, CefString& exception) {
    NativeTypeList nativeArgs = ConvertCefToNative(arguments);
    NativeType result;
    try {
      retval = ConvertNativeToCef(_function->InvokeWithArgs(nativeArgs));
    } catch (std::exception& ex) {
      // @TODO: Differentiate between exceptions to report back to the UI and exceptions to leave un-caught
      exception = ex.what();
    }
    return true;
  }

  FunctionHandler* _function;

IMPLEMENT_REFCOUNTING(NativeFunctionHandler);
};

struct CefFunctionHandler : public FunctionHandler {
  CefFunctionHandler(CefRefPtr<CefV8Value> value)
  : FunctionHandler(value->GetFunctionName()), _value(value) {
    if (!CefV8Context::InContext()) ThrowException("Cannot create function type outside of v8 context.");

    //store the context that we are current in,
    //which was entered before ConvertCefToNative
    _context = CefV8Context::GetEnteredContext();
  }

  virtual NativeType InvokeWithArgs(const NativeTypeList& args) const override {
    //we cannot continue if the associated context for a function has been released
    if (!_context->IsValid()) ThrowException("Function's context no longer exists.");

    return ExecuteOnUIT<NativeType>([&]{
      ContextScope scope(_context);
      return ConvertCefToNative(_value->ExecuteFunction(CefV8Value::CreateNull(), ConvertNativeToCef(args)));
    });
  }

  CefRefPtr<CefV8Context> _context;
  CefRefPtr<CefV8Value> _value;
};

std::string GetJSTypeString(CefRefPtr<CefV8Value> cef) {
  if (cef->IsInt()) return "int";
  if (cef->IsUInt()) return "uint";
  if (cef->IsDouble()) return "double";
  if (cef->IsBool()) return "bool";
  if (cef->IsString()) return "string";
  if (cef->IsArray()) return "array";
  if (cef->IsObject()) return "object";
  if (cef->IsFunction()) return "function";
  if (cef->IsNull()) return "null";
  if (cef->IsUndefined()) return "undefined";

  ThrowException("Cannot determine type of CefV8Value!?");
}

NativeType ConvertCefToNative(CefRefPtr<CefV8Value> cef) {
  if (!CefV8Context::InContext()) ThrowException("Cannot convert type outside of v8 context.");

  if (cef->IsInt()) return NativeType(cef->GetIntValue());
  if (cef->IsUInt()) return NativeType(cef->GetUIntValue());
  if (cef->IsDouble()) return NativeType(cef->GetDoubleValue());
  if (cef->IsBool()) return NativeType(cef->GetBoolValue());
  if (cef->IsString()) return NativeType(cef->GetStringValue().ToString());
  if (cef->IsNull()) return NativeType::Null();
  if (cef->IsUndefined()) return NativeType::Undefined();
  if (cef->IsFunction()) return NativeType(Function(new CefFunctionHandler(cef)));

  // NOTE: this check *NEEDS* to happen before cef->IsObject(),
  // because apparently an array literal is an object as well as an array
  if (cef->IsArray()) {
    Array arr;
    int len = cef->GetArrayLength();
    arr.GetVector().resize(len);

    for (int i=0; i<len; i++) {
      arr.Set(i, ConvertCefToNative(cef->GetValue(i)));
    }
    return NativeType(arr);
  }

  // An object is a mapping of string => value
  // Iterate over all the keys of the object, adding them to the internal map of the Object
  if (cef->IsObject()) {
    std::vector<CefString> keys;
    cef->GetKeys(keys);

    Object obj;

    for (auto key : keys) {
      obj.Set(key.ToString(), ConvertCefToNative(cef->GetValue(key)));
    }

    return NativeType(obj);
  }

  ThrowException("Cannot convert CefV8Value of type "+GetJSTypeString(cef)+" to NativeType");
}

NativeTypeList ConvertCefToNative(const CefV8ValueList& cef) {
  NativeTypeList out(cef.size());
  std::transform(cef.begin(), cef.end(), out.begin(), [](CefRefPtr<CefV8Value> cef){
    return ConvertCefToNative(cef);
  });
  return out;
}

CefRefPtr<CefV8Value> ConvertNativeToCef(NativeType native) {
  REQUIRE_UI_THREAD();
  if (native.IsInt()) return CefV8Value::CreateInt(native.GetInt());
  if (native.IsUInt()) return CefV8Value::CreateUInt(native.GetUInt());
  if (native.IsDouble()) return CefV8Value::CreateDouble(native.GetDouble());
  if (native.IsBool()) return CefV8Value::CreateBool(native.GetBool());
  if (native.IsString()) return CefV8Value::CreateString(native.GetString());
  if (native.IsNull()) return CefV8Value::CreateNull();
  if (native.IsUndefined()) return CefV8Value::CreateUndefined();
  if (native.IsFunction()) return CefV8Value::CreateFunction(native.GetFunction().Name(), new NativeFunctionHandler(native.GetFunction().GetHandler()));

  if (native.IsArray()) {
    auto vec = native.GetArray().GetVector();
    CefRefPtr<CefV8Value> cef (CefV8Value::CreateArray(vec.size()));

    for (int i=0; i<vec.size(); i++) {
      cef->SetValue(i, ConvertNativeToCef(vec.at(i)));
    }
  
    return cef;
  }

  if (native.IsObject()) {
    CefRefPtr<CefV8Value> cef (CefV8Value::CreateObject(nullptr));
    auto object = native.GetObject();

    for (auto kv : object.GetMap()) {
      cef->SetValue(kv.first, ConvertNativeToCef(kv.second), V8_PROPERTY_ATTRIBUTE_NONE);
    }

    return cef;
  }

  ThrowException("Cannot convert NativeType of type "+native.GetTypeString()+" to CefV8Value");
}

CefV8ValueList ConvertNativeToCef(const NativeTypeList& native) {
  REQUIRE_UI_THREAD();
  CefV8ValueList out(native.size());
  std::transform(native.begin(), native.end(), out.begin(), [](const NativeType& native){
    return ConvertNativeToCef(native);
  });
  return out;
}

CefRefPtr<CefV8Value> CallCefFunction(CefRefPtr<CefV8Context> context, const std::string& name, const CefV8ValueList& args) {
  REQUIRE_UI_THREAD();

  if (context == nullptr) ThrowException("Error: context does not exist.");
  if (name.size() == 0) ThrowException("Error: Cannot call function with empty name");

  ContextScope scope(context);

  auto window = context->GetGlobal();

  if (!window->HasValue("app")) ThrowException("Error: window.app does not exist");

  auto app = window->GetValue("app");

  if (!app->HasValue(name)) ThrowException("Error: window.app."+name+" does not exist");

  auto function = app->GetValue(name);

  if (!function->IsFunction()) ThrowException("Error: window.app."+name+" is not a function");

  auto ret = function->ExecuteFunction(nullptr, args);

  if (ret == nullptr) ThrowException("Error: window.app."+name+" encountered an exception");
  if (ret->IsObject() && ret->HasException()) ThrowException(ret->GetException()->GetMessage());

  return ret;
}

CefRefPtr<CefV8Value> GetOrCreateObject(const CefString& name, CefRefPtr<CefV8Value> parent) {
  CefRefPtr<CefV8Value> object;

  if (!parent->HasValue(name)) {
    object = CefV8Value::CreateObject(NULL);
    parent->SetValue(name, object, V8_PROPERTY_ATTRIBUTE_NONE);
  } else {
    object = parent->GetValue(name);
  }

  return object;
}

void AttachBindings(CefRefPtr<CefV8Value> object, std::map<std::string, JS::NativeType> bindings) {
  for (auto b : bindings) {
    object->SetValue(
      b.first,
      JS::ConvertNativeToCef(b.second),
      V8_PROPERTY_ATTRIBUTE_NONE
    );
  }
};

} //namespace JS
} //namespace Alchemy