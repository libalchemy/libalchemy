#include "include/cef_v8.h"

//must go below CEF includes because a GetObject macro is redefining our function
#include "alchemy/js.h"
#include "alchemy/js_utils.h"

#include "boost/variant.hpp"

#include <stdexcept>
#include <string>
#include <future>

namespace Alchemy {
namespace JS {

struct SafeBool {
  SafeBool(bool val) : _val(val) { };
  operator bool() { return _val; }
  bool _val;
};

typedef boost::variant<
  NativeType::UndefinedType,
  NativeType::NullType,
  int,
  unsigned int,
  double,
  SafeBool,
  std::string,
  Function,
  Object,
  Array
> NativeTypeVariant;

struct NativeType::Impl {
  NativeTypeVariant variant;

  template<typename T>
  Impl(const T& initial) : variant(initial) { }
};

NativeType NativeType::Null() {
  return NativeType(NativeType::NullType());
}

NativeType NativeType::Undefined() {
  return NativeType(NativeType::UndefinedType());
}

NativeType::NativeType()
: _impl(new NativeType::Impl(UndefinedType())) { }

NativeType::NativeType(const NativeType& other)
: _impl(new NativeType::Impl(other._impl->variant)) { }

NativeType::NativeType(NativeType&& other)
: _impl(std::move(other._impl)) { }

NativeType::NativeType(const NullType& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(const UndefinedType& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(int value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(unsigned int value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(double value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(bool value)
: _impl(new NativeType::Impl(SafeBool(value))) { }

NativeType::NativeType(const char* value)
: _impl(new NativeType::Impl(std::string(value))) { }

NativeType::NativeType(const std::string& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(const JS::Function& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(const Object& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::NativeType(const Array& value)
: _impl(new NativeType::Impl(value)) { }

NativeType::~NativeType() { }

NativeType& NativeType::operator=(const NativeType& other) {
  if (this == &other) return *this;

  _impl->variant = other._impl->variant;
  return *this;
}

// Move assignment is more efficent than copy assignment
// Use it when we're assigning an rvalue instead of an lvalue
NativeType& NativeType::operator=(NativeType&& other) {
  if (this == &other) return *this;

  // take ownership of other's _impl
  std::swap(_impl, other._impl);

  // then deallocate our original _impl
  other._impl.release();

  return *this;
}

std::string NativeType::GetTypeString() const {
  switch (_impl->variant.which()) {
    case 0: return "undefined";
    case 1: return "null";
    case 2: return "int";
    case 3: return "uint";
    case 4: return "double";
    case 5: return "bool";
    case 6: return "string";
    case 7: return "function";
    case 8: return "object";
    case 9: return "array";
  }
  ThrowException("Variant contains unknown type. Panic!");
}

std::string NativeType::GetDebugString() const {
  std::string type = GetTypeString();
  switch (_impl->variant.which()) {
    case 0:
    case 1:
      return "["+type+"]";
    case 2:
      return Mkstr()<<"["<<type<<" "<<GetInt()<<"]";
    case 3:
      return Mkstr()<<"["<<type<<" "<<GetUInt()<<"]";
    case 4:
      return Mkstr()<<"["<<type<<" "<<GetDouble()<<"]";
    case 5:
      return Mkstr()<<"["<<type<<" "<<(GetBool()? "true" : "false")<<"]";
    case 6:
      return Mkstr()<<"["<<type<<" \""<<GetString()<<"\"]";
    case 7:
      return Mkstr()<<"["<<type<<" "<<GetFunction().Name()<<"]";
    case 8:
      return Mkstr()<<"["<<type<<" "<<GetObject().GetMap().size()<<" keys]";
    case 9:
      return Mkstr()<<"["<<type<<" length="<<GetArray().GetVector().size()<<"]";
  }
  ThrowException("Don't know how to represent " + type + ". Panic!");
}

bool NativeType::IsUndefined() const { return _impl->variant.which() == 0; }
bool NativeType::IsNull() const      { return _impl->variant.which() == 1; }
bool NativeType::IsInt() const       { return _impl->variant.which() == 2; }
bool NativeType::IsUInt() const      { return _impl->variant.which() == 3; }
bool NativeType::IsDouble() const    { return _impl->variant.which() == 4; }
bool NativeType::IsBool() const      { return _impl->variant.which() == 5; }
bool NativeType::IsString() const    { return _impl->variant.which() == 6; }
bool NativeType::IsFunction() const  { return _impl->variant.which() == 7; }
bool NativeType::IsObject() const    { return _impl->variant.which() == 8; }
bool NativeType::IsArray() const     { return _impl->variant.which() == 9; }

bool NativeType::IsTruthy() const {
  struct visitor : public boost::static_visitor<bool> {
    bool operator()(NativeType::UndefinedType _) { return false; }
    bool operator()(NativeType::NullType _) { return false; }
    bool operator()(int val) { return val != 0; }
    bool operator()(unsigned int val) { return val != 0; }
    bool operator()(double val) { return val != 0.0; }
    bool operator()(SafeBool val) { return val._val; }
    bool operator()(const std::string& val) { return val.size()!=0; }
    bool operator()(const Function& val) { return true; }
    bool operator()(const Object& val) { return true; }
    bool operator()(const Array& val) { return true; }
  };
  visitor v;
  return boost::apply_visitor(v, _impl->variant);
}

int NativeType::GetInt() const            { return boost::get<int>(_impl->variant); }
unsigned int NativeType::GetUInt() const  { return boost::get<unsigned int>(_impl->variant); }
double NativeType::GetDouble() const      { return boost::get<double>(_impl->variant); }
bool NativeType::GetBool() const          { return boost::get<SafeBool>(_impl->variant); }
std::string NativeType::GetString() const { return boost::get<std::string>(_impl->variant); }
Function NativeType::GetFunction() const  { return boost::get<JS::Function>(_impl->variant); }
Object NativeType::GetObject() const      { return boost::get<Object>(_impl->variant); }
Array NativeType::GetArray() const        { return boost::get<Array>(_impl->variant); }

NativeType::operator int() const {
  if (!IsInt()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to int");
  return GetInt();
}
NativeType::operator unsigned int() const {
  if (!IsUInt()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to unsigned int");
  return GetUInt();
}
NativeType::operator double() const {
  if (!IsDouble()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to double");
  return GetDouble();
}
NativeType::operator bool() const {
  if (!IsBool()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to bool");
  return GetBool();
}
NativeType::operator std::string() const {
  if (!IsString()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to std::string");
  return GetString();
}
NativeType::operator JS::Function() const {
  if (!IsFunction()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to FunctionHandler*");
  return GetFunction();
}
NativeType::operator Object() const {
  if (!IsObject()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to Object");
  return GetObject();
}
NativeType::operator Array() const {
  if (!IsArray()) ThrowException("Cannot cast NativeType " + GetDebugString() + " to Array");
  return GetArray();
}

NativeType&& Or(NativeType&& a, NativeType&& b) {
  return a.IsTruthy()? std::forward<NativeType>(a) : std::forward<NativeType>(b);
}

Function::Function(const Function& other)
: _handler(other._handler) { }

Function::Function(Function&& other)
: _handler(std::move(other._handler)) { }

Function::Function(FunctionHandler* other)
: _handler(other) { }

Function::~Function() { }

Function& Function::operator=(const Function& other) {
  _handler = other._handler;
  return *this;
}

Function& Function::operator=(Function&& other) {
  _handler = std::move(other._handler);
  return *this;
}

FunctionHandler* Function::GetHandler() {
  return _handler.get();
}

Object::Object() { }

Object::Object(const Object& other)
: _map(other._map) { }

Object::Object(Object&& other)
: _map(std::move(other._map)) { }

Object::Object(const MapType& map)
: _map(map) { }

Object::~Object() { }

Object& Object::operator=(const Object& other) {
  _map = other._map;
  return *this;
}

Object& Object::operator=(Object&& other) {
  std::swap(_map, other._map);
  return *this;
}

void Object::Set(const std::string& key, const NativeType& value) {
  auto it = _map.find(key);
  if (it == _map.end()) {
    _map.insert(std::make_pair(key, value));
  } else {
    it->second = value;
  }
}

NativeType Object::Get(const std::string& key) const {
  auto it = _map.find(key);
  if (it == _map.end()) return NativeType::Undefined();
  return it->second;
}

Object::MapType& Object::GetMap() {
  return _map;
}

const Object::MapType& Object::GetMap() const {
  return _map;
}

Object Object::Merge(const Object& a, const Object& b) {
  Object out = a;
  for (auto kv : b.GetMap()) {
    out.Set(kv.first, kv.second);
  }
  return out;
}

Array::Array() { }

Array::Array(const Array& other)
: _vector(other._vector) { }

Array::Array(Array&& other)
: _vector(std::move(other._vector)) { }

Array::Array(const VectorType& map)
: _vector(map) { }

Array::~Array() { }

Array& Array::operator=(const Array& other) {
  _vector = other._vector;
  return *this;
}

Array& Array::operator=(Array&& other) {
  std::swap(_vector, other._vector);
  return *this;
}

void Array::Set(int index, const NativeType& value) {
  // JS arrays are interesting; you can do
  //   var arr = [1,2,3];
  //   arr[25] = 'foo';
  // arr would then equal [1,2,3, undefined...x22, 'foo']
  // Since NativeType's default constructor creates undefined values,
  // we just need to resize the vector to match
  if (index >= _vector.size()) {
    _vector.resize(index+1);
  }
  _vector.at(index) = value;
}

NativeType Array::Get(int index) const {
  // If the index is out of bounds, it should return null
  //   var arr = [1,2,3];
  //   arr[23]; //=> undefined
  //   arr; //=> [1,2,3];
  if (index >= _vector.size()) {
    return NativeType::Undefined();
  } else {
    return _vector.at(index);
  }
}

Array::VectorType& Array::GetVector() {
  return _vector;
}

const Array::VectorType& Array::GetVector() const {
  return _vector;
}

int Array::Length() const {
  return _vector.size();
}

} // namespace JS
} // namespace Alchemy