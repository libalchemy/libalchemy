#ifndef ALCHEMY_PAGE_H
#define ALCHEMY_PAGE_H
#pragma once

#include <string>
#include <map>
#include <future>
#include <functional>

#include "alchemy/application.h"
#include "alchemy/page_handler.h"
#include "alchemy/client.h"
#include "alchemy/js.h"

namespace Alchemy {


class Page {
public:
  /**
  * Constructs a new Page object given the client and URL for the actual file.
  *
  *@param[in] Client object
  *@param[in] URL to file location
  */
  Page(Client* client, const std::string& url);

  /**
  * Associates page handler to page object.
  *
  *@param[in] PageHandler object to associate
  */
  void SetHandler(const PageHandler& ph);

  /**
  * Returns the URL of the file associated with this page object.
  */
  std::string GetURL();

  /**
  * Returns whether the page has finished loading and its JavaScript context has been created.
  */
  bool Ready();

  /**
  * Binds a native C++ function (global function or static member function) to the JavaScript window.app object
  *
  *@param[in] The name of the function to be bound to window.app.name
  *@param[in] The function pointer to be invoked
  */
  template <typename F>
  void Bind(const std::string& name, F fp) {
    std::lock_guard<std::mutex> lock(_lock);
    _jsObj.Set(name, JS::Function(name, fp));
  }

  /**
  * Binds a native C++ function (member function) to the JavaScript window.app object
  *
  *@param[in] The name of the function to be bound to window.app.name
  *@param[in] The member function pointer to be invoked, such as &Class::function
  *@param[in] The object owning the member function to be invoked
  */
  template <typename T, typename F>
  void Bind(const std::string& name, F fp, T obj) {
    std::lock_guard<std::mutex> lock(_lock);
    _jsObj.Set(name, JS::Function(name, fp, obj));
  }

  /**
  * Calls a function on the JavaScript window.app object
  *
  *@param[in] The name of the function bound to window.app.name
  *@param[in] The parameters passed to the function
  */
  template<typename... ParameterTypes>
  JS::NativeType Call(const std::string& name, const ParameterTypes&... parameters) {
    return Call(name, CollectArgs<JS::NativeType>(parameters...));
  }

  /**
  * Returns the JavaScript object to use as the window.app object
  */
  JS::Object GetBindings();

  friend class ClientImpl; //private method access only

private:
  /** Client object associated with this page */
  Client* _client;
  /** Map of function names to Javascript functions */
  JS::Object _jsObj;
  /** URL of file associated with this page */
  std::string _url;
  /** Whether the page is loaded or not */
  bool _isLoaded;
  /** Whether the page has a JavaScript context or not */ 
  bool _hasContext;
  /** Mutex for thread-safe member access */   
  std::mutex _lock;

  /**
  * Set whether the page is loaded
  *
  *@param[in] bool value
  */
  void SetIsLoaded(bool isLoaded);

  /**
  * Set whether the page has a JavaScript context
  *
  *@param[in] bool value
  */
  void SetHasContext(bool hasContext);

  /**
  * Calls a function on the JavaScript window.app object
  *
  *@param[in] The name of the function bound to window.app.name
  *@param[in] The NativeTypeList of parameters passed to the function
  */
  JS::NativeType Call(const std::string& name, const JS::NativeTypeList& parameters);
};

} // namespace Alchemy

#endif // ALCHEMY_PAGE_H
