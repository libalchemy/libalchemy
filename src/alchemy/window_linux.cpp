#include <csignal>
#include <string>
#include <gtk/gtk.h>
#include "alchemy/window.h"
#include "alchemy/application.h"

namespace Alchemy {
namespace UI {

void Window::GlobalInit() {
  gtk_init(NULL, NULL);
}

void destroy(void) {
  CefQuitMessageLoop();
}

void signalHandler(int status) {
  destroy();
}

void Window::Init(int width, int height) {
  _handle = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(_handle), width, height);

  g_signal_connect(G_OBJECT(_handle), "destroy", G_CALLBACK(gtk_widget_destroyed), &_handle);
  g_signal_connect(G_OBJECT(_handle), "destroy", G_CALLBACK(destroy), NULL);
  std::signal(SIGINT, signalHandler);
  std::signal(SIGTERM, signalHandler);
}

void Window::Load(ClientImpl* client, Page* page) {
  GtkWidget* vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(_handle), vbox);

  CefWindowInfo info;
  CefBrowserSettings settings;

  info.SetAsChild(vbox);
  CefBrowser::CreateBrowserSync(info, client, page->GetURL(), settings);
}

void Window::Show() {
  gtk_widget_show_all(GTK_WIDGET(_handle));
}

void Window::Close() {
  gtk_widget_destroy((GtkWidget*)_handle);
}

int Window::ScreenWidth() {
  GdkScreen* screen = gdk_screen_get_default();
  return gdk_screen_get_width(screen);
}

int Window::ScreenHeight() {
  GdkScreen* screen = gdk_screen_get_default();
  return gdk_screen_get_height(screen);
}

int Window::TitleBarHeight() {
  // TODO: figure out how to calculate this
  return 0;
}

void Window::Minimize() {
  gtk_window_iconify((GtkWindow*)_handle);
}

void Window::Maximize() {
  gtk_window_maximize((GtkWindow*)_handle);
}

void Window::SetTitle(const std::string& title) {
  gtk_window_set_title((GtkWindow*)_handle, title.c_str());
}

void Window::Restore() {
  gint state = gdk_window_get_state(_handle->window);
  if (state & GDK_WINDOW_STATE_FULLSCREEN) {
    gtk_window_unfullscreen((GtkWindow*)_handle);
  } else if (state & GDK_WINDOW_STATE_MAXIMIZED) {
    gtk_window_unmaximize((GtkWindow*)_handle);
  } else if (state & GDK_WINDOW_STATE_ICONIFIED) {
    gtk_window_deiconify((GtkWindow*)_handle);
  }
}

void Window::SetPosition(int x, int y) {
  GtkWindow* window = (GtkWindow*)_handle;
  gtk_window_move(window, x, y);
}

void Window::SetSize(int width, int height) {
  GtkWindow* window = (GtkWindow*)_handle;
  gtk_window_resize(window, width, height);
}

int Window::GetX() {
  gint x;
  gtk_window_get_position((GtkWindow*)_handle, &x, NULL);
  return x;
}

int Window::GetY() {
  gint y;
  gtk_window_get_position((GtkWindow*)_handle, NULL, &y);
  return y;
}

int Window::GetWidth() {
  gint width;
  gtk_window_get_size((GtkWindow*)_handle, &width, NULL);
  return width;
}

int Window::GetHeight() {
  gint height;
  gtk_window_get_size((GtkWindow*)_handle, NULL, &height);
  return height;
}

}}
