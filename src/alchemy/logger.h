#ifndef ALCHEMY_LOGGER_H
#define ALCHEMY_LOGGER_H
#pragma once

#include <string>
#include <ostream>
#include "utils.h"

namespace Alchemy {

class Logger {
public:
  Logger(std::ostream& stream);
  void Info(const std::string& msg);
  void Warning(const std::string& msg);
  void Error(const std::string& msg);

private:
  std::ostream& _stream;
};

Logger& GetLogger();

} // namespace Alchemy

#endif