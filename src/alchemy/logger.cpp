#include "alchemy/logger.h"
#ifdef DEBUG
  #include <iostream>
#else
  #include <fstream>
#endif

namespace Alchemy {

Logger::Logger(std::ostream& stream)
: _stream(stream) {}

void Logger::Info(const std::string& msg) {
  _stream << "INFO: " << msg << std::endl;
}
void Logger::Warning(const std::string& msg) {
  _stream << "WARNING: " << msg << std::endl;
}
void Logger::Error(const std::string& msg) {
  _stream << "ERROR: " << msg << std::endl;
}

Logger& GetLogger() {
#ifdef DEBUG
  static std::ostream& _ostream = std::clog;
#else
  static std::ofstream _ostream (GetExecutableFolder().append("/alchemy.log"), std::ios::app);
#endif
  static Logger log (_ostream);
  return log;
}

} // namespace Alchemy