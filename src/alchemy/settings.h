#ifndef ALCHEMY_SETTINGS_H
#define ALCHEMY_SETTINGS_H
#pragma once

#include <string>
#include "alchemy/utils.h"

#define _SETTINGS_LIST(macro) \
  macro(std::string, Directory) \
  macro(Alchemy::Size, DefaultWindowSize) \
  macro(bool, ShowDevTools)

#define _SETTINGS_METHOD(type, name) \
  type name() const { return _##name; } \
  void name(const type& value) { _##name = value; }

#define _SETTINGS_PROPERTY(type, name) type _##name;

namespace Alchemy {

class Settings {
public:
  Settings() {
    Directory("ui");
    DefaultWindowSize(Alchemy::Size(800, 600));
    ShowDevTools(false);
  }

  _SETTINGS_LIST(_SETTINGS_METHOD)

private:
  _SETTINGS_LIST(_SETTINGS_PROPERTY)
};

} // namespace Alchemy

#endif