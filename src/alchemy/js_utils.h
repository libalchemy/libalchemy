#ifndef ALCHEMY_JS_UTILS_H
#define ALCHEMY_JS_UTILS_H
#pragma once

#include <iostream>
#include <future>

#include "include/cef_v8.h"
#include "include/cef_runnable.h"
#include "include/cef_task.h"

//must go below CEF includes because a GetObject macro is redefining our function
#include "alchemy/utils.h"
#include "alchemy/js.h"

namespace Alchemy {
namespace JS {

#define REQUIRE_UI_THREAD() if (!CefCurrentlyOn(TID_UI)) ThrowException("Can only call __FUNCTION__ on UI thread!")

std::string GetJSTypeString(CefRefPtr<CefV8Value> cef);

/**
* Convenience structure for handling work in a context.
*/
struct ContextScope {
  ContextScope(CefRefPtr<CefV8Context> context)
  : _context(context), _alreadyIn(context->InContext()) {
    if (!_alreadyIn) _context->Enter();
  };
  ~ContextScope() {
    if (!_alreadyIn) _context->Exit();
  }

  CefRefPtr<CefV8Context> _context;
  bool _alreadyIn;
};

template <typename R, typename F, typename... Args>
R ExecuteOnUIT(F f, Args&&... args) {
  if (CefCurrentlyOn(TID_UI)) {
    return f(std::forward<Args>(args)...);
  }

  std::promise<R> promise;
  std::future<R> future = promise.get_future();

// Windows doesn't like the bind with a parameter pack
// but Linux won't compile with a parameter pack in a lambda
// soooo... use an ifdef and be done with it
#ifdef __LINUX__
  auto bound = std::bind(f, std::forward<Args>(args)...);
  auto function = [&]{ promise.set_value(bound()); };
#else
  auto function = [&]{ promise.set_value(f(std::forward<Args>(args)...)); };
#endif

  CefPostTask(TID_UI, NewCefRunnableFunction(&function));

  return future.get();
}

//helper to get or create objects on a parent object
/**
* Helper function to get or create objects on a parent object.
*
*@param[in] Object name
*@param[in] Parent object
*/
CefRefPtr<CefV8Value> GetOrCreateObject(const CefString& name, CefRefPtr<CefV8Value> parent);

void AttachBindings(CefRefPtr<CefV8Value> object, std::map<std::string, JS::NativeType> bindings);

CefRefPtr<CefV8Value> CallCefFunction(CefRefPtr<CefV8Context> context, const std::string& name, const CefV8ValueList& args);

NativeType ConvertCefToNative(CefRefPtr<CefV8Value> cef);
NativeTypeList ConvertCefToNative(const CefV8ValueList& cef);
CefRefPtr<CefV8Value> ConvertNativeToCef(NativeType native);
CefV8ValueList ConvertNativeToCef(const NativeTypeList& native);

} //namespace JS
} //namespace Alchemy

#endif //ALCHEMY_JS_UTILS_H