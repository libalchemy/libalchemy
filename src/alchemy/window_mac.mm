// Parts taken from the AppJS project
#import <Cocoa/Cocoa.h>
#include <string>

#include "alchemy/window.h"

#include "include/cef_client.h"
#include "include/cef_application_mac.h"

static NSAutoreleasePool* g_autopool = nil;

const int TITLEBAR_HEIGHT = 22;

@interface CocoaMultiThreading : NSObject
+ (void)beginMultiThreading;
@end

@implementation CocoaMultiThreading
+ (void) dummyThread:(id)unused {
  (void)unused;
}

+ (void) beginMultiThreading {
  [NSThread detachNewThreadSelector:@selector(dummyThread:)
            toTarget:self
            withObject:nil
  ];
}
@end

// Provide the CefAppProtocol implementation required by CEF.
@interface AlchemyApplication : NSApplication<CefAppProtocol> {
@private
  BOOL handlingSendEvent_;
}
@end

@implementation AlchemyApplication
- (BOOL)isHandlingSendEvent {
  return handlingSendEvent_;
}

- (void)setHandlingSendEvent:(BOOL)handlingSendEvent {
  handlingSendEvent_ = handlingSendEvent;
}

- (void)sendEvent:(NSEvent*)event {
  CefScopedSendingEvent sendingEventScoper;
  [super sendEvent:event];
}

@end

@interface AlchemyWindow : NSWindow
@end
@implementation AlchemyWindow
-(BOOL)canBecomeKeyWindow {
  return YES;
}
-(BOOL)canBecomeMainWindow {
  return YES;
}
@end

@interface AlchemyWindowDelegate : NSObject <NSWindowDelegate>
@end
@implementation AlchemyWindowDelegate
- (BOOL)windowShouldClose:(id)window {
  // Try to make the window go away.
  [window autorelease];

  // Clean ourselves up after clearing the stack of anything that might have the
  // window on it.
  [self performSelectorOnMainThread:@selector(cleanup:)
                         withObject:window
                      waitUntilDone:NO];

  return YES;
}

// Deletes itself.
- (void)cleanup:(id)window {
  [self release];
}
@end

@interface AlchemyAppDelegate : NSObject
@end
@implementation AlchemyAppDelegate
// Sent by the default notification center immediately before the application
// terminates.
- (void)applicationWillTerminate:(NSNotification *)aNotification {
  [self release];
  [g_autopool release];
}
@end

namespace Alchemy {
namespace UI {

void Window::GlobalInit() {
  [CocoaMultiThreading beginMultiThreading];
  g_autopool = [[NSAutoreleasePool alloc] init];

  [AlchemyApplication sharedApplication];
  AlchemyAppDelegate* appDelegate = [[AlchemyAppDelegate alloc] init];
  [NSApp setDelegate: appDelegate];
  [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];

  id menubar = [[NSMenu new] autorelease];
  id appMenuItem = [[NSMenuItem new] autorelease];
  [menubar addItem:appMenuItem];
  [NSApp setMainMenu:menubar];

  id appMenu = [[NSMenu new] autorelease];
  id appName = [[NSProcessInfo processInfo] processName];
  id quitTitle = [@"Quit " stringByAppendingString:appName];
  id quitMenuItem = [[[NSMenuItem alloc] initWithTitle:quitTitle
      action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
  [appMenu addItem:quitMenuItem];
  [appMenuItem setSubmenu:appMenu];
}

void Window::Init(int width, int height) {
  NSRect rect = NSMakeRect(0,0,width,height-TITLEBAR_HEIGHT);
  NSWindow* window = [[AlchemyWindow alloc]
                       initWithContentRect: rect
                                 styleMask: NSTitledWindowMask
                                          | NSClosableWindowMask
                                          | NSMiniaturizableWindowMask
                                          | NSResizableWindowMask
                                   backing: NSBackingStoreBuffered
                                     defer: NO];
  [window setReleasedWhenClosed: YES];
  [window setBackgroundColor: [NSColor clearColor]];
  [window setDelegate: [[AlchemyWindowDelegate alloc] init]];

  _handle = [window contentView];
}

void Window::Load(ClientImpl* client, Page* page) {
  CefWindowInfo info;
  CefBrowserSettings settings;

  info.SetAsChild(_handle, 0, 0, GetWidth(), GetHeight() - TitleBarHeight());
  CefBrowser::CreateBrowser(info, client, page->GetURL(), settings);
}

void Window::Show() {
  [[_handle window] makeKeyAndOrderFront: nil];
}

void Window::Close() {
  [[_handle window] performClose: nil];
}

int Window::ScreenWidth() {
  NSRect screen_rect = [[NSScreen mainScreen] visibleFrame];
  return screen_rect.size.width;
}

int Window::ScreenHeight() {
  NSRect screen_rect = [[NSScreen mainScreen] visibleFrame];
  return screen_rect.size.height;
}

int Window::TitleBarHeight() {
  return TITLEBAR_HEIGHT;
}

void Window::Minimize() {
  [[_handle window]
    performSelectorOnMainThread: @selector(performMiniaturize:)
                     withObject: nil
                  waitUntilDone: NO];
}

void Window::Maximize() {
  [[_handle window]
    performSelectorOnMainThread: @selector(performZoom:)
                     withObject: nil
                  waitUntilDone: NO];
}

void Window::Restore() {
  NSWindow* win = [_handle window];
  if( [win isZoomed] ){
    [win performSelectorOnMainThread:@selector(zoom:)
                         withObject:nil
                      waitUntilDone:NO];
}

  if( [win isMiniaturized]) {
    [win performSelectorOnMainThread:@selector(deminiaturize:)
                         withObject:nil
                      waitUntilDone:NO];
  }
}

void Window::SetTitle(const std::string& title) {
  [[_handle window] setTitle: [NSString stringWithUTF8String:title.c_str()]];
}

void Window::SetSize(int width, int height) {
  NSWindow* win = [_handle window];
  NSRect windowRect = [win frame];
  windowRect.size.width = width;
  windowRect.size.height = height-TITLEBAR_HEIGHT;
  [win setFrame:[win frameRectForContentRect: windowRect] display:YES];
}

void Window::SetPosition(int x, int y) {
  NSRect r = [[_handle window] frame];
  int screen_h = [[NSScreen mainScreen] visibleFrame].size.height;

  [[_handle window] setFrameOrigin: NSMakePoint(x, screen_h - r.size.height - y)];
}

int Window::GetX() {
  return (int)[[_handle window] frame].origin.x;
}

int Window::GetY() {
  // Mac's origin is in the bottom left rather than top left
  int screen_h = [[NSScreen mainScreen] visibleFrame].size.height;
  NSRect r = [[_handle window] frame];
  return (int)(screen_h - r.size.height - r.origin.y);
}

int Window::GetWidth() {
  return (int)[[_handle window] frame].size.width;
}

int Window::GetHeight() {
  return (int)[[_handle window] frame].size.height;
}

}}