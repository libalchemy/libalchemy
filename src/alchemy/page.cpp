#include "alchemy/page.h"
#include "alchemy/client.h"
#include "alchemy/js.h"

namespace Alchemy {

Page::Page(Client* client, const std::string& url)
  : _client(client), _url(url), _isLoaded(false), _hasContext(false) { }

std::string Page::GetURL() {
  return _url;
}

bool Page::Ready() {
  std::lock_guard<std::mutex> lock(_lock);

  return _isLoaded && _hasContext;
}

void Page::SetIsLoaded(bool isLoaded) {
  std::lock_guard<std::mutex> lock(_lock);

  _isLoaded = isLoaded;
}

void Page::SetHasContext(bool hasContext) {
  std::lock_guard<std::mutex> lock(_lock);

  _hasContext = hasContext;
}

JS::Object Page::GetBindings() {
  std::lock_guard<std::mutex> lock(_lock);
  return _jsObj;
}

JS::NativeType Page::Call(const std::string& name, const JS::NativeTypeList& args) {
  return _client->CallFunction(this, name, args);
}

} // namespace Alchemy