#ifndef ALCHEMY_UTILS_H
#define ALCHEMY_UTILS_H
#pragma once

#include <sstream>
#include <string>
#include <memory>
#include <vector>

// complementary to std::make_shared, just constructs a unique_ptr in place
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

#ifdef __WIN__
  #define FILE_PROTOCOL "file:///"
#else
  #define FILE_PROTOCOL "file://"
#endif

#ifdef __WIN__
  //MSVC
  #define NORETURN __declspec(noreturn)
#else
  //Clang, GCC
  #define NORETURN __attribute__((noreturn))
#endif

namespace Alchemy {

template <typename T>
void _CollectArgs(std::vector<T>& vec) { }

template <typename T, typename Head, typename... Tail>
void _CollectArgs(std::vector<T>& vec, const Head& h, const Tail&... tail) {
  vec.push_back(T(h));
  _CollectArgs<T, Tail...>(vec, tail...);
}

template <typename T, typename... Args>
std::vector<T> CollectArgs(const Args&... args) {
  std::vector<T> out;
  _CollectArgs<T, Args...>(out, args...);
  return out;
}

/**
* Struct to represent window height and width.
* Also provides methods to set size.
*/
struct Size {
  Size() : width(800), height(600) { }
  Size(int w, int h) : width(w), height(h) { }

  int width, height;
};

/**
 * String formatting utility
 *
 * Usage: std::string s = Mkstr() << "5 + 5 = " << 5+5 << " for example";
 */
class Mkstr {
public:
  template <typename T>
  Mkstr& operator<< (const T& rhs) {
    ss_ << rhs;
    return *this;
  }
  operator std::string () const {
    return ss_.str();
  }
  //operator const char* () const {
  //  return ss_.str().c_str();
  //}
private:
  std::ostringstream ss_;
};

/**
* Gets the current executable folder and returns it as a string.
*/
std::string GetExecutableFolder();

/**
* Prints given error message to CER and then throws it.
*
*@param[in] Error message to print
*/
NORETURN void ThrowException(std::string msg);

} // namespace Alchemy

#endif