#include "alchemy/application_impl.h"
#include "alchemy/client_impl.h"
#include "alchemy/window.h"
#include "alchemy/utils.h"
#include "alchemy/logger.h"

#include "include/cef_app.h"
#include "include/cef_runnable.h"
#include "include/cef_scheme.h"
#include "include/cef_task.h"

#include <string>

namespace Alchemy {

ApplicationImpl::ApplicationImpl(const Settings& settings)
: _client(make_unique<ClientImpl>(this)), _window(settings), _settings(settings) {
  GetLogger().Info("Pre CefInitialize");

  CefInitialize(CefSettings(), nullptr);

  GetLogger().Info("Post CefInitialize");
}

ApplicationImpl::~ApplicationImpl() {
  GetLogger().Info("Pre CefShutdown");

  CefShutdown();

  GetLogger().Info("Post CefShutdown");
}

void ApplicationImpl::Show(const std::string& name) {
  Show(this->Page(name));
}

void ApplicationImpl::Show(Alchemy::Page* page) {
  GetLogger().Info("Loading window with page url " + page->GetURL());
  _window.Load(_client.get(), page);

  _window.Show();

  GetLogger().Info("Pre CefRunMessageLoop");
  CefRunMessageLoop();
  GetLogger().Info("Post CefRunMessageLoop");
}

void ApplicationImpl::Close() {
  auto function = []{ 
    GetLogger().Info("Pre CefQuitMessageLoop");
    CefQuitMessageLoop();
    GetLogger().Info("Post CefQuitMessageLoop");
  };
  CefPostTask(TID_UI, NewCefRunnableFunction(&function));
}

Page* ApplicationImpl::Page(const std::string& name) {
  std::lock_guard<std::mutex> lock(_lock);

  PageMap::iterator it = _pages.find(name);

  if (it == _pages.end()) {
    it = _pages.insert(std::make_pair(name, make_unique<Alchemy::Page>(_client.get(), GetURLPrefix().append(name)))).first;
  }

  return it->second.get();
}

Page* ApplicationImpl::GetPageByURL(const std::string& url) {
  std::string prefix = GetURLPrefix();
  std::string name;

  size_t index = url.find(prefix);
  if (index == 0) {
    name = url.substr(prefix.length());
  } else {
    return nullptr;
  }

  std::lock_guard<std::mutex> lock(_lock);
  auto it = _pages.find(name);
  return it == _pages.end()? nullptr : it->second.get();
}

bool ApplicationImpl::PageExists(std::string pageName) {
  std::lock_guard<std::mutex> lock(_lock);
  return _pages.find(pageName) != _pages.end();
}

std::string ApplicationImpl::GetURLPrefix() {
  return std::string(FILE_PROTOCOL).append(GetExecutableFolder()).append("/").append(_settings.Directory()).append("/");
}

UI::Window ApplicationImpl::GetWindow() {
  return _window;
}

bool ApplicationImpl::IsDevToolsShown() {
  return _client->IsDevToolsShown();
}

Alchemy::Settings ApplicationImpl::GetSettings() {
  return _settings;
}

} // namespace Alchemy