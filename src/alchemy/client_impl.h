#ifndef ALCHEMY_CLIENT_IMPL_H
#define ALCHEMY_CLIENT_IMPL_H
#pragma once

#include "alchemy/client.h"
#include "alchemy/js.h"
#include "alchemy/utils.h"

#include "include/cef_client.h"
#include "include/cef_app.h"

#include <mutex>
#include <string>

namespace Alchemy {

class ApplicationImpl;
class Page;

class ClientImpl : public Client, public CefClient, public CefV8ContextHandler, public CefLoadHandler, public CefLifeSpanHandler {
public:
  /**
  * Constructs a new ClientImpl object.
  *
  *@param[in] ApplicationImpl object
  */
  ClientImpl(ApplicationImpl* app);
  /**
  * ApplicationImpl destructor
  */
  ~ClientImpl();

  /** Track if Dev Tools is shown */
  bool IsDevToolsShown();

  // Client
  virtual JS::NativeType CallFunction(Page* page, const std::string& name, const JS::NativeTypeList& args) OVERRIDE;

  // CefClient
  virtual CefRefPtr<CefV8ContextHandler> GetV8ContextHandler() OVERRIDE { return this; }
  virtual CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE { return this; }
  virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() OVERRIDE { return this; }

  // CefV8ContextHandler
  virtual void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) OVERRIDE;
  virtual void OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) OVERRIDE;

  // CefLoadHandler
  virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode) OVERRIDE;

  // CefLifeSpanHandler
  virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;

private:
  /** ApplicationImpl object */
  ApplicationImpl* _app;
  /** Map of string type names to Javascript native types */
  JS::Object _jsAPI;
  /** Map of string names to contexts */
  std::map<std::string, CefRefPtr<CefV8Context>> _contexts;
  /** Mutex for thread-safe member access */
  std::mutex _lock;
  /** Flag to track client initialization */
  bool _initialized;
  /** Testing flag to see if dev tools has been shown */
  bool _isDevToolsShown;

  /**
  * Returns the page specified by the given URL.
  *
  *@param[in] URL of the page to return
  */
  Page* GetPage(const std::string& url);
  /**
  * Returns the context associated with the given page.
  *
  *@param[in] Page to get context from
  */
  CefRefPtr<CefV8Context> GetContextForPage(Page* page);
  /**
  * Stores the page/context pair.
  *
  *@param[in] Page object to store
  *@param[in] Context to stroe
  */
  void StorePageContext(Page* page, CefRefPtr<CefV8Context> context);
  /**
  * Clears all contexts that match to the given page.
  *
  *@param[in] Page object to clear all contexts associated with
  */
  void ClearPageContext(Page* page);

public:
  int AddRef() { return refct_.AddRef(); }
  int Release() {
    int retval = refct_.Release();
    // if (retval == 0)
    //   delete this;
    return retval;
  }
  int GetRefCt() { return refct_.GetRefCt(); }
private:
  CefRefCount refct_;
};

} // namespace Alchemy

#endif // ALCHEMY_CLIENT_IMPL_H