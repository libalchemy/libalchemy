#include "alchemy/client_impl.h"

#include <future>
#include <stdexcept>
#include <string>

#include "alchemy/application_impl.h"
#include "alchemy/page.h"
#include "alchemy/js.h"
#include "alchemy/js_utils.h"
#include "alchemy/logger.h"

namespace Alchemy {

//register Window member function
#define REGISTER_JS_API(name, function) \
  _jsAPI.Set(name, JS::Function(name, &UI::Window::function, _app->GetWindow()));

//register Window static function
#define REGISTER_JS_API_STATIC(name, function) \
  _jsAPI.Set(name, JS::Function(name, &UI::Window::function));

ClientImpl::ClientImpl (ApplicationImpl* app) : _app(app), _initialized(false), _isDevToolsShown(false) { }

ClientImpl::~ClientImpl() { }

JS::NativeType ClientImpl::CallFunction(Page* page, const std::string& name, const JS::NativeTypeList& args) {
  auto context = GetContextForPage(page);

  return JS::ExecuteOnUIT<JS::NativeType>([&]{
    JS::ContextScope scope(context);
    return JS::ConvertCefToNative(JS::CallCefFunction(context, name, JS::ConvertNativeToCef(args)));
  });
}

void ClientImpl::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {
  auto page = _app->GetPageByURL(frame->GetURL().ToString());

  if (page != nullptr) {
    StorePageContext(page, context);
    page->SetHasContext(true);

    JS::ContextScope scope(context);

    auto global = context->GetGlobal();

    // Page bindings
    auto app = JS::ConvertCefToNative(JS::GetOrCreateObject("app", global));
    auto merged = JS::Object::Merge(app, page->GetBindings());
    global->SetValue("app", JS::ConvertNativeToCef(merged), V8_PROPERTY_ATTRIBUTE_NONE);

    auto frame = JS::ConvertCefToNative(JS::GetOrCreateObject("frame", global));
    merged = JS::Object::Merge(frame, _jsAPI);
    global->SetValue("frame", JS::ConvertNativeToCef(merged), V8_PROPERTY_ATTRIBUTE_NONE);
  } else {
    GetLogger().Warning("Context created for url '" + frame->GetURL().ToString() + "', but no corresponding page was registered with Application");
  }
}

void ClientImpl::OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode) {
  //set the title from pages as they are loaded
  //only set the title from the main frame (not embedded iframes)
  if (frame->IsMain()) {
    auto context = frame->GetV8Context();
    JS::ContextScope scope(context);

    auto title = context->GetGlobal()->GetValue("document")->GetValue("title")->GetStringValue().ToString();
    _app->GetWindow().SetTitle(title);
  }

  auto page = GetPage(frame->GetURL().ToString());

  if (page != nullptr) page->SetIsLoaded(true);
}

void ClientImpl::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
  if (_initialized) return; //only supports one window

  if (_app->GetSettings().ShowDevTools()) {
    GetLogger().Info("Showing Developer Tools for url " + browser->GetMainFrame()->GetURL().ToString());
    browser->ShowDevTools();
    _isDevToolsShown = true;
  }

  GetLogger().Info("About to register Window API for url " + browser->GetMainFrame()->GetURL().ToString());

  REGISTER_JS_API("minimize",    Minimize)
  REGISTER_JS_API("maximize",    Maximize)
  REGISTER_JS_API("restore",     Restore)
  REGISTER_JS_API("setTitle",    SetTitle)
  REGISTER_JS_API("setSize",     SetSize)
  REGISTER_JS_API("setPosition", SetPosition)
  REGISTER_JS_API("getX",        GetX)
  REGISTER_JS_API("getY",        GetY)
  REGISTER_JS_API("getWidth",    GetWidth)
  REGISTER_JS_API("getHeight",   GetHeight)

  REGISTER_JS_API_STATIC("getScreenWidth",  ScreenWidth)
  REGISTER_JS_API_STATIC("getScreenHeight", ScreenHeight)

  _initialized = true;
}

void ClientImpl::OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) {
  auto page = GetPage(frame->GetURL().ToString());

  if (page != nullptr) {
    ClearPageContext(page);
    page->SetHasContext(false);
    page->SetIsLoaded(false);
  } else {
    GetLogger().Warning("Context released for url '" + frame->GetURL().ToString() + "', but no corresponding page was registered with Application");
  }
}

Page* ClientImpl::GetPage(const std::string& url) {
  return _app->GetPageByURL(url);
}

CefRefPtr<CefV8Context> ClientImpl::GetContextForPage(Page* page) {
  std::lock_guard<std::mutex> lock(_lock);

  if (page == nullptr) {
    GetLogger().Error("In ClientImpl::GetContextForPage(), null page pointer was passed");
    ThrowException("Cannot retrieve context for a null page");
  }
  
  std::string url = page->GetURL();
  auto it = _contexts.find(url);

  if (it == _contexts.end()) {
    GetLogger().Error("In ClientImpl::GetContextForPage(), could not find a corresponding context for url " + url);
    ThrowException("Could not find a corresponding context for " + url);
  }

  return it->second;
}

void ClientImpl::StorePageContext(Page* page, CefRefPtr<CefV8Context> context) {
  std::lock_guard<std::mutex> lock(_lock);

  if (page == nullptr) {
    GetLogger().Error("In ClientImpl::StorePageContext(), null page pointer was passed");
    ThrowException("Cannot save context for a null page");
  }

  std::string url = page->GetURL();
  if (!_contexts.insert(std::make_pair(url, context)).second) {
    GetLogger().Error("In ClientImpl::StorePageContext(), a context already exists for url " + url);
    ThrowException("Cannot overwrite context for " + url);
  }
}

void ClientImpl::ClearPageContext(Page* page) {
  std::lock_guard<std::mutex> lock(_lock);

  if (page == nullptr) {
    GetLogger().Error("In ClientImpl::ClearPageContext(), null page pointer was passed");
    ThrowException("Cannot clear context for a null page");
  }

  std::string url = page->GetURL();
  auto it = _contexts.find(url);

  if (it == _contexts.end()) {
    GetLogger().Error("In ClientImpl::ClearPageContext(), could not find a corresponding context for url " + url);
    ThrowException("Could not find a corresponding context for " + url);
  }

  it->second = nullptr;

  if (_contexts.erase(url) == 0) {
    GetLogger().Error("In ClientImpl::ClearPageContext(), stored context was not erased for url " + url);
    ThrowException("Did not erase context for " + url);
  }
}

bool ClientImpl::IsDevToolsShown() {
  return _isDevToolsShown;
}
} //namespace Alchemy
