#include <string>
#include <unistd.h>
#include "alchemy/utils.h"

namespace Alchemy {

// Finds the current file source path and reaturns it as a string
// returns an empty string on failure
std::string GetExecutableFolder() {
  char path_buff[1024];

  size_t len = readlink("/proc/self/exe", path_buff, sizeof(path_buff)-1);
  if (len == -1)
    return "";

  // null terminate
  path_buff[len] = 0;

  // chop off the executable filename
  std::string dir = std::string(path_buff);
  size_t lastslash = dir.rfind("/");
  return lastslash == std::string::npos ? "" : dir.substr(0, lastslash).append("/");
}


}
