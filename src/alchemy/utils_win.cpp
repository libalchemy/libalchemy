#include "alchemy/utils.h"
#include "windows.h"
#include <algorithm>
#include <string>

namespace Alchemy {

// Finds the current file source path and reaturns it as a string
// returns an empty string on failure
std::string GetExecutableFolder() {
  char szPath[MAX_PATH];
  std::string path;
  int index;

  if(!GetModuleFileNameA(NULL, szPath, MAX_PATH)) {
    return "";
  }

  path = szPath;
  index = path.find_last_of('\\');
  path = index != path.npos ? path.substr(0, index + 1) : "";

  std::replace(path.begin(), path.end(), '\\', '/');
  return path;
}

} //namespace Alchemy