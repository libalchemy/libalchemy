#include "gtest/gtest.h"

#include "alchemy/alchemy.h"
#include "alchemy/js.h"

#include <thread>
#include <chrono>
#include <cmath>

using namespace std;

namespace Test {
namespace function_binding {

std::unique_ptr<Alchemy::Application> app;
bool bindingsInvoked = false;

struct Foo {
  int StringLength(string s) {
    return s.length();
  }

  static double Square(int num) {
    return sqrt(num);
  }
};

void test() {
  bindingsInvoked = true;
}

int add(int num1, int num2) {
  return num1 + num2;
}

string retrieveFunction(Alchemy::JS::NativeType func) {
  return func.GetDebugString();
}

void executeBindings() {
  auto index = app->Page("index.html");

  while(!index->Ready()) {
    this_thread::sleep_for(chrono::milliseconds(100));
  }

  //regular function
  index->Call("test");
  EXPECT_EQ(bindingsInvoked, true);

  //function with parameters
  int sum = index->Call("add", 3, 4);
  EXPECT_EQ(sum, 7);

  //member function
  int len = index->Call("stringLength", "test");
  EXPECT_EQ(len, 4);

  //static member function
  int square = index->Call("square", 64);
  EXPECT_EQ(square, 8.0);

  auto anonymousFunction = "app.retrieveFunction(function(){});";
  auto logFunction =       "app.retrieveFunction(console.log);";
  string name;

  name = string(index->Call("eval", anonymousFunction));
  EXPECT_EQ(name, "[function ]"); //anonymous function
  name = string(index->Call("eval", logFunction));
  EXPECT_EQ(name, "[function log]");

  app->Close();
}

TEST(JS, TestFunctionBindings) {
  Foo f;
  app = Alchemy::Application::Create(Alchemy::Settings());

  std::thread t(executeBindings);

  auto index = app->Page("index.html");
  index->Bind("test", test);
  index->Bind("add", add);
  index->Bind("stringLength", &Foo::StringLength, f);
  index->Bind("square", Foo::Square);
  index->Bind("retrieveFunction", retrieveFunction);

  app->Show(index);

  t.join();
  app.release();
}

}
}