#include "gtest/gtest.h"

#include "alchemy/alchemy.h"
#include "alchemy/js.h"

#include <thread>
#include <chrono>

using namespace std;

namespace Test {
namespace function_calling {

std::unique_ptr<Alchemy::Application> app;
int expected = 123;

int echo(int number) {
  return number;
}

void executeFunction() {
  stringstream expression;
  auto index = app->Page("index.html");

  while(!index->Ready()) {
    this_thread::sleep_for(chrono::milliseconds(100));
  }

  expression << "app.echo(" << expected << ");";
  int received = index->Call("eval", expression.str());
  EXPECT_EQ(received, expected);

  app->Close();
}

TEST(JS, TestFunctionCalling) {
  app = Alchemy::Application::Create(Alchemy::Settings());

  std::thread t(executeFunction);

  auto index = app->Page("index.html");
  index->Bind("echo", echo);

  app->Show(index);

  t.join();
  app.release();
}

}
}