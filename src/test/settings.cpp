#include "gtest/gtest.h"

#include "alchemy/alchemy.h"
#include "alchemy/js.h"

#include <thread>
#include <chrono>

using namespace std;

namespace Test {
namespace settings {

std::unique_ptr<Alchemy::Application> app;
int expected_width = 300, expected_height = 400;

void executeFunction() {
  auto index = app->Page("index.html");

  while(!index->Ready()) {
    this_thread::sleep_for(chrono::milliseconds(100));
  }

  //objects need the context in ConvertCefToNative
  Alchemy::JS::Object sizeObj = index->Call("getWindowSize");
  EXPECT_EQ(sizeObj.Get("width").GetInt(), expected_width);
  EXPECT_EQ(sizeObj.Get("height").GetInt(), expected_height);

  app->Close();
}

TEST(Settings, WindowSize) {
  Alchemy::Settings settings;

  settings.DefaultWindowSize(Alchemy::Size(expected_width, expected_height));
  app = Alchemy::Application::Create(settings);

  std::thread t(executeFunction);

  auto index = app->Page("index.html");
  app->Show(index);

  t.join();
  app.release();
}

}
}