#include "gtest/gtest.h"

#include "alchemy/logger.h"

#include <iostream>
#include <sstream>

using namespace std;

namespace Test {
namespace ErrorLogging {

TEST(TestErrorLogging, TestInfoMessage) {
  std::ostringstream stream;
  Alchemy::Logger log(stream);
  log.Info("This is an info message");
  EXPECT_EQ("INFO: This is an info message\n", stream.str());
}

TEST(TestErrorLogging, TestWarningMessage) {
  std::ostringstream stream;
  Alchemy::Logger log(stream);
  log.Warning("This is a warning message");
  EXPECT_EQ("WARNING: This is a warning message\n", stream.str());
}

TEST(TestErrorLogging, TestErrorMessage) {
  std::ostringstream stream;
  Alchemy::Logger log(stream);
  log.Error("This is an error message");
  EXPECT_EQ("ERROR: This is an error message\n", stream.str());
}

}
}