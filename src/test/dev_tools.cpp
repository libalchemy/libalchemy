#include "gtest/gtest.h"

#include "alchemy/alchemy.h"
#include "alchemy/js.h"

#include <thread>
#include <chrono>

using namespace std;

namespace Test {
namespace dev_tools {

std::unique_ptr<Alchemy::Application> app;

void executeFunction(bool expected) {
  auto index = app->Page("index.html");

  while(!index->Ready()) {
    this_thread::sleep_for(chrono::milliseconds(100));
  }

  bool received = app->IsDevToolsShown();
  EXPECT_EQ(received, expected);

  app->Close();
}

TEST(DevToolsTest, ShowsDevTools) {
  Alchemy::Settings settings;

  settings.ShowDevTools(true);
  app = Alchemy::Application::Create(settings);

  std::thread t(executeFunction, true);

  auto index = app->Page("index.html");

  app->Show(index);

  t.join();
  app.release();
}

TEST(DevToolsTest, NoDevTools) {
  app = Alchemy::Application::Create(Alchemy::Settings());

  std::thread t(executeFunction, false);

  auto index = app->Page("index.html");

  app->Show(index);

  t.join();
  app.release();
}

}
}