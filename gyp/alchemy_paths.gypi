{
  'variables': {
    'alchemy_path': '../src',
    'test_path': '../src/test',

    # headers for the libalchemy library
    'alchemy_includes_common': [
      '<(alchemy_path)/alchemy/alchemy.h',
      '<(alchemy_path)/alchemy/application.h',
      '<(alchemy_path)/alchemy/client.h',
      '<(alchemy_path)/alchemy/logger.h',
      '<(alchemy_path)/alchemy/page.h',
      '<(alchemy_path)/alchemy/page_handler.h',
      '<(alchemy_path)/alchemy/settings.h',
      '<(alchemy_path)/alchemy/utils.h',

      '<(alchemy_path)/alchemy/js.h',

      '<(alchemy_path)/alchemy/ui_types.h',
      '<(alchemy_path)/alchemy/window.h',
    ],

    'alchemy_includes_impl': [
      '<(alchemy_path)/alchemy/application_impl.h',
      '<(alchemy_path)/alchemy/client_impl.h',
      '<(alchemy_path)/alchemy/js_utils.h',
    ],

    # platform independent sources for the libalchemy library
    'alchemy_sources_common': [
      '<(alchemy_path)/alchemy/application.cpp',
      '<(alchemy_path)/alchemy/application_impl.cpp',
      '<(alchemy_path)/alchemy/client_impl.cpp',
      '<(alchemy_path)/alchemy/logger.cpp',
      '<(alchemy_path)/alchemy/page.cpp',
      '<(alchemy_path)/alchemy/utils.cpp',

      '<(alchemy_path)/alchemy/js.cpp',
      '<(alchemy_path)/alchemy/js_utils.cpp',

      '<(alchemy_path)/alchemy/window.cpp',
    ],

    # platform dependent sources for the libalchemy library
    'alchemy_sources_linux': [
      '<(alchemy_path)/alchemy/window_linux.cpp',
      '<(alchemy_path)/alchemy/utils_linux.cpp',
    ],
    'alchemy_sources_mac': [
      '<(alchemy_path)/alchemy/window_mac.mm',
      '<(alchemy_path)/alchemy/utils_mac.mm',
    ],
    'alchemy_sources_win': [
      '<(alchemy_path)/alchemy/window_win.cpp',
      '<(alchemy_path)/alchemy/utils_win.cpp',
    ],

    # test paths
    'alchemy_tests': [
      '<(test_path)/function_calling.cpp',
      '<(test_path)/function_binding.cpp',
      '<(test_path)/settings.cpp',
      '<(test_path)/dev_tools.cpp',
      '<(test_path)/error_logging.cpp',
    ]
  }
}
